// =============================================================================
// Include files 
// =============================================================================
// STD & STL 
// =============================================================================
#include <cmath>
// =============================================================================
// GaudiKernel
// =============================================================================
#include "GaudiKernel/ToStream.h"
// =============================================================================
// GaudiAlg 
// =============================================================================
#include "GaudiAlg/GaudiHistoTool.h"
// =============================================================================
// GaudiUtils
// =============================================================================
#include "GaudiUtils/Aida2ROOT.h"
// =============================================================================
// AIDA 
// =============================================================================
#include "AIDA/IHistogram2D.h"
// =============================================================================
// Generators 
// =============================================================================
#include "MCInterfaces/IGenCutTool.h"
// =============================================================================
// PartProp
// =============================================================================
#include "Kernel/iNode.h"
// =============================================================================
// LHCbMath
// =============================================================================
#include "LHCbMath/LHCbMath.h"
#include "LHCbMath/ValueWithError.h"
// =============================================================================
// LoKi
// =============================================================================
#include "LoKi/Objects.h"
#include "LoKi/Primitives.h"
#include "LoKi/GenTypes.h"
#include "LoKi/GenParticles.h"
#include "LoKi/IGenDecay.h"
#include "LoKi/IGenHybridFactory.h"
#include "LoKi/Trees.h"
#include "LoKi/PrintHepMCDecay.h"
#include "LoKi/GenDecayChain.h"
#include "LoKi/GenOscillated.h"
#include "LoKi/PrintHepMCDecay.h"
#include "LoKi/GenDecayChain.h"
#include "LoKi/ParticleProperties.h"
// =============================================================================
// ROOT
// =============================================================================
#include "TH2D.h"
#include "TAxis.h"
// =============================================================================
// Boost
// =============================================================================
#include "boost/algorithm/string/join.hpp"
// =============================================================================
// Local
// =============================================================================
#include "GenericGenCutTool.h"
// =============================================================================
namespace 
{
  // ===========================================================================
  /// the invalid tree 
  const Decays::IGenDecay::Tree s_TREE = 
    Decays::Trees::Invalid_<const HepMC::GenParticle*>() ;
  /// the invalid node 
  const Decays::Node            s_NODE = Decays::Nodes::Invalid() ;
  /// the invalid function 
  const LoKi::GenTypes::GFun    s_FUNC = 
    LoKi::BasicFunctors<const HepMC::GenParticle*>::Constant ( -1 )  ;
  /// the invalid predicate 
  const LoKi::GenTypes::GCut    s_PRED = 
    LoKi::BasicFunctors<const HepMC::GenParticle*>::BooleanConstant ( false )  ;
  // ===========================================================================
} //                                                  end of anonymous namespace 
// =============================================================================
// constructor 
// =============================================================================
LoKi::GenCutTool::TwoCuts::TwoCuts 
( const Decays::iNode&         c1 ,
  const LoKi::GenTypes::GCuts& c2 ) 
  : first   ( c1      ) 
  , second  ( c2      ) 
  , counter ( 0       ) 
{}
// =============================================================================
// constructor 
// =============================================================================
LoKi::GenCutTool::TwoCuts::TwoCuts ()  
  : first   ( s_NODE )  
  , second  ( s_PRED ) 
  , counter ( 0 ) 
{}
// =============================================================================
/* standard constructor 
 *  @param type the tool type (???)
 *  @param name the actual instance name 
 *  @param parent the tool parent 
 */
// =============================================================================
LoKi::GenCutTool::GenCutTool 
( const std::string&  type   ,                     // the tool type (???)
  const std::string&  name   ,                     // the tool isntance name 
  const IInterface*   parent )                     // the tool parent
// : base_class ( type , name , parent ) 
  : GaudiHistoTool ( type , name , parent ) 
// 
  , m_descriptor ( "<Invaild-Decay-Descriptor>" ) 
  , m_finder     ( s_TREE )
  , m_cuts       () 
  , m_criteria   () 
  , m_preambulo  () 
  , m_factory    ( "LoKi::Hybrid::GenTool:PUBLIC" ) 
  , m_fill_counters ( true   ) 
  , m_filter        ( false  ) 
  , m_xaxis      ( 0.0 , 10.0 , 20 , "GPT/GeV" ) // transverse momentum 
  , m_yaxis      ( 2.0 ,  5.0 ,  6 , "GY"      ) // rapidity
  , m_x          ( s_FUNC )
  , m_y          ( s_FUNC )
//
  , m_histo1 ( 0 ) 
  , m_histo2 ( 0 ) 
  , m_histo3 ( 0 ) 
//
{
  //
  declareInterface<IGenCutTool>( this ) ;
  //
  declareProperty 
    ( "Decay"           ,
      m_descriptor      , 
      "The decay descriptor (with marked particles) to be tested" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateDescriptor  , this ) ;
  //
  m_cuts [ "StableCharged" ] = " in_range ( 0.010 , GTHETA , 0.400 ) " ;
  m_cuts [ "Gamma"         ] = " in_range ( 0.005 , GTHETA , 0.400 ) " ;
  declareProperty 
    ( "Cuts"      , 
      m_cuts      , 
      "The map with selection criteria : { Node : Cuts } " ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateCuts  , this ) ;
  //
  m_preambulo.push_back("from GaudiKernel.SystemOfUnits import MeV,GeV,mm,cm") ;
  declareProperty 
    ( "Preambulo" , 
      m_preambulo , 
      "Preambulo to be used for LoKi/Bender Hybrid factory" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateCuts  , this ) ;
  //
  declareProperty 
    ( "Factory"   , 
      m_factory   , 
      "The type/name for LoKi/Bender Hybrid factory" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateCuts  , this ) ;
  // 
  declareProperty 
    ( "Filter"   , 
      m_filter   , 
      "Use decay descriptor to filter decays?" ) ;
  //
  declareProperty 
    ( "XAxis"     , m_xaxis , "X-axis for efficiency histogram" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateHistos  , this ) ;
  //
  declareProperty 
    ( "YAxis"     , m_yaxis , "Y-axis for efficiency histogram" ) 
    -> declareUpdateHandler 
    ( &LoKi::GenCutTool::updateHistos  , this ) ;
  //
  declareProperty 
    ( "FillCounters" , m_fill_counters , "Fill efficiency counters (useful for debug)" ) ;
  // ===========================================================================
}
// =============================================================================
// destructor 
// =============================================================================
LoKi::GenCutTool::~GenCutTool() {}
// =============================================================================
// update-handler for the property "DecayDescriptor" 
// =============================================================================
void LoKi::GenCutTool::updateDescriptor ( Property& /* p */ ) 
{
  // no action if not yet initialized 
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  // 
  m_update_decay = true ;
}
// =============================================================================
// update-handler for the properties "Cuts","Preambulo","Factory" 
// =============================================================================
void LoKi::GenCutTool::updateCuts       ( Property& /* p */ ) 
{
  // no action if not yet initialized 
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  // 
  m_update_cuts   = true ;
  m_update_histos = true ;
}
// =============================================================================
// update-handler for the properties "XAxis","YAxis"
// =============================================================================
void LoKi::GenCutTool::updateHistos     ( Property& /* p */ ) 
{
  // no action if not yet initialized 
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return ; }
  // 
  m_update_histos = true ;
}
// =============================================================================
// decode the decay descriptor 
// =============================================================================
StatusCode LoKi::GenCutTool::decodeDescriptor ()  const 
{
  //
  m_update_decay = true ;
  // get the factory:
  Decays::IGenDecay* factory = tool<Decays::IGenDecay>("LoKi::GenDecay", this ) ;
  // use the factory:
  Decays::IGenDecay::Tree tree = factory->tree ( m_descriptor ) ;
  if ( !tree ) 
  { return Error( "Unable to decode the descriptor '" + m_descriptor + "'") ; }
  //
  m_finder = Decays::IGenDecay::Finder ( tree ) ;
  if ( !m_finder ) 
  { return Error( "Unable to create the finder '" + m_descriptor + "'") ; }
  //
  m_update_decay = false ;
  //
  release ( factory ) ;
  //
  return StatusCode::SUCCESS ;
} 
// =============================================================================
// decode cuts 
// =============================================================================
StatusCode LoKi::GenCutTool::decodeCuts ()  const 
{
  //
  m_update_cuts = true ;
  // 
  m_criteria.clear() ;
  //
  // get the factory:
  LoKi::IGenHybridFactory* factory = tool<LoKi::IGenHybridFactory>( m_factory , this ) ;
  // get the factory:
  Decays::IGenDecay*       nodes   = tool<Decays::IGenDecay>("LoKi::GenDecay" , this ) ;
  // 
  // decode cuts :
  for ( const auto& entry : m_cuts ) 
  {
    TwoCuts item ;
    // use node-factory
    item.first  = nodes->node ( entry.first ) ;
    if ( !item.first ) { return Error ("Unable to decode the node '" + entry.first + "'") ; }
    // use the cut-factory:
    StatusCode sc = factory->get 
      ( entry.second    , 
        item .second    , 
        preambulo ()    ) ;
    if ( sc.isFailure() ) { return Error ("Unable to decode the cut '" + entry.second+ "'", sc ) ; }
    //
    m_criteria.push_back ( item ) ;
  }
  //
  m_update_cuts = false ;
  //
  release ( nodes   ) ;
  release ( factory ) ;
  // 
  return StatusCode::SUCCESS ;
}
// =============================================================================
// decode Histos 
// =============================================================================
StatusCode LoKi::GenCutTool::decodeHistos ()  const 
{
  m_update_histos = true  ;
  // reset histos 
  if ( 0 != m_histo1 ) { m_histo1 -> reset() ; m_histo1 = 0 ; }
  if ( 0 != m_histo2 ) { m_histo2 -> reset() ; m_histo2 = 0 ; }
  if ( 0 != m_histo3 ) { m_histo3 -> reset() ; m_histo3 = 0 ; }
  // reset functions 
  m_x = s_FUNC ;
  m_y = s_FUNC ;
  //
  // aquire the factory: 
  LoKi::IGenHybridFactory* factory = 
    tool<LoKi::IGenHybridFactory>( m_factory , this ) ;
  //
  StatusCode sc = factory -> get ( m_xaxis.title() , 
                                   m_x             , 
                                   preambulo()     ) ;
  
  if ( sc.isFailure() ) 
  { return Error ("Unable to decode the X-axis '" + m_xaxis.title() + "'", sc ) ; }
  sc = factory -> get ( m_yaxis.title() , 
                        m_y             , 
                        preambulo()     ) ;
  if ( sc.isFailure() ) 
  { return Error ("Unable to decode the Y-axis '" + m_xaxis.title() + "'", sc ) ; }
  
  if ( produceHistos() ) 
  {
    m_histo1 = book2D 
      ( "All"               , 
        m_descriptor        , 
        //
        m_xaxis.lowEdge  () , 
        m_xaxis.highEdge () , 
        m_xaxis.bins     () , 
        //
        m_yaxis.lowEdge  () , 
        m_yaxis.highEdge () , 
        m_yaxis.bins     () ) ; 
    //
    m_histo2 = book2D 
      ( "Accepted"          , 
        m_descriptor        , 
        //
        m_xaxis.lowEdge  () , 
        m_xaxis.highEdge () , 
        m_xaxis.bins     () , 
        //
        m_yaxis.lowEdge  () , 
        m_yaxis.highEdge () , 
        m_yaxis.bins     () ) ; 
    //
    m_histo3 = book2D 
      ( "Efficiency"        , 
        m_descriptor        , 
        //
        m_xaxis.lowEdge  () , 
        m_xaxis.highEdge () , 
        m_xaxis.bins     () , 
        //
        m_yaxis.lowEdge  () , 
        m_yaxis.highEdge () , 
        m_yaxis.bins     () ) ; 
  }
  // ==========================================================================
  m_update_histos = false ;
  release ( factory ) ;
  // 
  return StatusCode::SUCCESS ;
}
// =============================================================================
// intialize the tool 
// =============================================================================
StatusCode LoKi::GenCutTool::initialize() 
{
  // initialize the base 
  StatusCode sc = GaudiHistoTool::initialize() ;
  if ( sc.isFailure() ) { return sc ; }
  //
  // decode the decay descriptor 
  sc = decodeDescriptor () ;
  if ( sc.isFailure() ) 
  { return Error ( "Unable to decode the descriptor '"+m_descriptor+"'", sc ) ; }
  //
  // decode the cuts
  sc = decodeCuts       () ;
  if ( sc.isFailure() ) { return Error ( "Unable to decode cuts"   , sc ) ; }
  //
  // decode the histos 
  sc = decodeHistos     () ;
  if ( sc.isFailure() ) { return Error ( "Unable to decode histos" , sc ) ; }
  //
  if ( ( statPrint() || msgLevel ( MSG::DEBUG ) ) && !m_fill_counters )
  { m_fill_counters = true ; }
  //
  if ( m_fill_counters )
  {
    for ( auto& cut : m_criteria ) 
    {
      StatEntity& cnt = counter( "Efficiency for " +  cut.first.toString () ) ;
      cut.counter    = &cnt ;
    }
  }
  //
  return StatusCode::SUCCESS ;
}
// =============================================================================
// finalize the tool 
// =============================================================================
StatusCode LoKi::GenCutTool::finalize () 
{
  /// calculate the efficiency 
  getEfficiency() ;
  /// check errors/problems 
  if  ( m_fill_counters ) 
  {
    for ( const auto& cut : m_criteria ) 
    {
      if ( !cut.counter ) { continue ; }
      if      ( 0 == cut.counter->nEntries() )
      { Error   ( "Cut for " +  cut.first.toString() + " is defined, but not used").ignore() ; }
      else if ( 0 == cut.counter->sum     () )
      { Warning ( "No passed events for " +  cut.first.toString() + " cut" ).ignore() ; }
    } 
  }
  /// finalize the base class 
  return GaudiHistoTool::finalize () ;
}
// =============================================================================
// accept the particle 
// =============================================================================
bool LoKi::GenCutTool::accept ( const HepMC::GenParticle* particle ) const 
{
  /// check the argument 
  if ( 0 == particle ) { return false ; }                    // RETURN
  //
  if ( m_filter )
  {
    const bool matched = m_finder.tree() ( particle ) ;
    counter ( "#accept_decay" ) += matched ;
    if ( !matched ) 
    {
      Warning ("The decay is not matched", StatusCode::FAILURE, 10 ).ignore() ;
      if ( UNLIKELY ( msgLevel ( MSG::DEBUG ) ) )
      { debug () << "the  decay is not-matched:" 
                 <<  LoKi::Print::printDecay ( particle ) << endmsg ; }
      return false ;
    }
  }
  //
  bool accepted = true ;
  LoKi::GenTypes::GenContainer particles ;
  m_finder.findDecay ( LoKi::GenTypes::GenContainer ( 1 , particle ) , particles ) ;
  std::reverse ( particles.begin() , particles.end() ) ;
  if ( UNLIKELY ( msgLevel ( MSG::DEBUG ) ) ) 
  {
    MsgStream& stream = debug () ;
    stream << "Selected marked particles (" << particles.size() << ") : ";
    LoKi::GenDecayChain printer{} ;
    printer.print ( particles.begin ()     ,
                    particles.end   ()     ,
                    stream                 ,
                    endmsg                 , 
                    LoKi::Objects::_VALID_ , // show 
                    LoKi::Objects::_NONE_  , // mark 
                    " " ,  0  ) << endmsg ;
  }
  if ( particles.empty() ) 
  { 
    const std::string decay  = LoKi::Print::printDecay ( particle )  ;
    Error   ( "No particles are selected from: " + decay ,   
              StatusCode::FAILURE , 20 ) . ignore() ;  
    if ( UNLIKELY ( msgLevel ( MSG::DEBUG ) ) ) 
    {
      debug () << "No particles are selected from the decay:" ;
      LoKi::GenDecayChain printer {} ;
      printer.print ( particle   , 
                      warning () ,
                      endmsg     , 
                      LoKi::Objects::_VALID_                          , // show  
                      LoKi::GenParticles::DecTree { m_finder.tree() } , // mark 
                      "  " , 0   ) << endmsg ;
    }
    return false ;                                        // RETURN 
  }
  // count them 
  counter ( "selected marked" ) += particles.size() ;
  // ===========================================================================
  // loop over the selected particles 
  // ===========================================================================
  for ( const HepMC::GenParticle* p : particles ) 
  {
    if ( 0 == p    ) { continue ; }               // CONTINUE 
    if ( !accepted ) { break    ; }               // BREAK 
    //
    const LHCb::ParticleID pid( p->pdg_id() ) ;
    //
    bool found = false ;
    for ( const auto& cut : m_criteria ) 
    {
      if ( !accepted           ) { break    ; }   // BREAK    
      if ( !cut.first  ( pid ) ) { continue ; }   // CONTINUE 
      //
      found    = true ;
      accepted = cut.second ( p ) ;
      if ( m_fill_counters && cut.counter )  
      {
        StatEntity& cnt = *(cut.counter) ;
        cnt += accepted ; 
      }
    }
    if ( !found ) 
    {
      const std::string  pname = LoKi::Particles::nameFromPID ( pid ) ;
      ++counter ( "no cuts found for " + pname );
      Error ( "No cuts are specified for selected particle " + pname ,
              StatusCode::FAILURE , 20 ) . ignore() ;
      if ( UNLIKELY ( msgLevel ( MSG::DEBUG ) ) ) 
      {
        debug () << "No cuts are specified for the selected particle " << pname << " :";
        LoKi::GenDecayChain printer{} ;
        printer.print 
          ( particle     , 
            error ()     , 
            endmsg       ,
            LoKi::Objects::_VALID_                         , // show  
            LoKi::TheSame<const HepMC::GenParticle*> { p } , // mark
              " " ,  0 )   <<  endmsg    ;
      }
    }
  }
  //
  // fill the histogram 
  //
  if ( 0 != m_histo1 && 0 != m_histo2 ) 
  {
    const double x = m_x ( particle ) ;
    const double y = m_y ( particle ) ;
    m_histo1 -> fill ( x , y , 1        ) ;
    m_histo2 -> fill ( x , y , accepted ) ;
  }
  //
  return accepted ;
} 
// ============================================================================
// construct preambulo string 
// ============================================================================
std::string LoKi::GenCutTool::preambulo() const    
{ return boost::algorithm::join( m_preambulo , "\n" ) ; }
// ============================================================================
namespace 
{
  // ==========================================================================
  /// integer number ?
  inline bool non_integer ( const double value ) 
  {
    return 
      !LHCb::Math::equal_to_double ( value , std::floor ( value ) ) ; 
  }
  // ==========================================================================
  /// evaluate the binomial effciency  
  inline Gaudi::Math::ValueWithError 
  efficiency ( const double n , const double N ) 
  {
    if ( 0 >= N ) { Gaudi::Math::ValueWithError ( 0 , 1 ) ; }
    //
    const double n1 = std::max ( n      , 1.0 ) ;
    const double n2 = std::max ( N - n  , 1.0 ) ;
    //
    return Gaudi::Math::ValueWithError ( n/N , n1*n2/(N*N*N) ) ;  
  }
  // ==========================================================================
}
// ============================================================================
// calculate the efficiency histo 
// ============================================================================
StatusCode LoKi::GenCutTool::getEfficiency() 
{
  //
  if ( 0 == m_histo1 || 0 == m_histo2 || 0 == m_histo3 ) 
  { return Error("Unable to calculate the efficiency") ; }
  //
  // reset the efficiency histo 
  m_histo3->reset() ;
  //
  TH2D* h1 = Gaudi::Utils::Aida2ROOT::aida2root ( m_histo1 ) ;
  TH2D* h2 = Gaudi::Utils::Aida2ROOT::aida2root ( m_histo2 ) ;
  TH2D* h3 = Gaudi::Utils::Aida2ROOT::aida2root ( m_histo3 ) ;
  //
  if ( 0 == h1 || 0 == h2 || 0 == h3 ) 
  { return Error ( "Unable to access ROOT historgams" ) ; }
  //
  TAxis* xaxis = h1 -> GetXaxis() ;
  TAxis* yaxis = h1 -> GetYaxis() ;
  //
  unsigned int bad_bins  = 0 ;
  unsigned int null_bins = 0 ;
  //
  for ( int ix = 1 ; ix <= xaxis->GetNbins() ; ++ix ) 
  {
    for ( int iy = 1 ; iy <= yaxis->GetNbins() ; ++iy ) 
    {
      const double N  = h1 -> GetBinContent ( ix , iy ) ;
      //
      if ( N < 0  ) // || !non_integer ( N )  ) 
      {
        Warning ( "Can't calculate the efficiency: illegal content N" ).ignore()  ;
        ++bad_bins ;
        continue ;
      }
      //
      const double n  = h2 -> GetBinContent ( ix , iy ) ;
      //
      if ( n < 0 || N < n ) //  || !non_integer ( N )  ) 
      {
        Warning ("Can't calculate the efficicency: illegal content n").ignore()  ;
        ++bad_bins ;
        continue ;
      }
      // 
      if ( 0 == N ) { ++null_bins ; }
      //
      Gaudi::Math::ValueWithError eff = efficiency ( n , N ) ;
      if ( eff.cov2() < 0 ) 
      {
        Warning ("Can't calculate the binomial efficiency").ignore ()  ;
        ++bad_bins ;
        continue ;
      }
      // 
      h3 -> SetBinContent ( ix , iy , eff.value () ) ;
      h3 -> SetBinError   ( ix , iy , eff.error () ) ;
    }
  }
  //
  if ( 0 < bad_bins || 0 < null_bins ) 
  { Warning("# of bad/null bins for efficiency histogram: " 
            + Gaudi::Utils::toString ( bad_bins  )  + "/"
            + Gaudi::Utils::toString ( null_bins )  , 
            StatusCode::FAILURE , 0 ) . ignore () ; }
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
// Accept events with 'good' particles 
// ============================================================================
bool LoKi::GenCutTool::applyCut
( ParticleVector&              particles       , 
  const HepMC::GenEvent*    /* theEvent     */ , 
  const LHCb::GenCollision* /* theCollision */ ) const 
{
  //
  StatEntity& cnt1 = counter ( "accept_particles" ) ;
  StatEntity& cnt2 = counter ( "accept_events"    ) ;
  //
  ParticleVector good ;
  good.reserve ( particles.size() ) ;
  //
  for ( HepMC::GenParticle* p : particles ) 
  {
    if ( 0 == p ) { continue ; }
    //
    const bool accepted = accept ( p ) ;
    if ( accepted ) { good.push_back ( p ) ; }
    cnt1 += accepted ;
  }
  //
  particles.clear() ;
  particles.insert ( particles.end() , good.begin () , good.end () ) ;
  //
  const bool accepted = !particles.empty() ;
  //
  cnt2 += accepted ;
  //
  return accepted ;
}
// ============================================================================
// factory
// ============================================================================
DECLARE_COMPONENT(LoKi::GenCutTool) 
// ============================================================================
// The END 
// ============================================================================
