################################################################################# Package: LbBound
################################################################################
gaudi_subdir(LbBound v1r2)

gaudi_depends_on_subdirs(Gen/Generators)

find_package(HepMC COMPONENTS fio)
find_package(ROOT COMPONENTS Hist Physics)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(LbBound
                 src/component/*.cpp
                 LINK_LIBRARIES GeneratorsLib HepMC ROOT)
