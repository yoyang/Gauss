################################################################################
# Package: GaussGeo
################################################################################
gaudi_subdir(GaussGeo v1r0)

gaudi_depends_on_subdirs(Det/DetDesc
                         Sim/GiGa)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussGeo
                 src/component/*.cpp
                 INCLUDE_DIRS Tools/ClhepTools
                 LINK_LIBRARIES DetDescLib GiGaLib)
