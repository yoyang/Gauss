################################################################################
# Package: LbGDML
################################################################################
gaudi_subdir(LbGDML v1r0p2)

gaudi_depends_on_subdirs(GaudiAlg
                         Sim/GaussTools)

FindG4libs(persistency)

find_package(XercesC)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(LbGDML
                 src/*.cpp
                 INCLUDE_DIRS XercesC
                 LINK_LIBRARIES GaudiAlgLib GaussToolsLib
                                XercesC
                                ${GEANT4_LIBS}
                 GENCONF_PRELOAD GaussToolsGenConfHelperLib)

