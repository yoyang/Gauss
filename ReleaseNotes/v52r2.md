

2018-06-24 Gauss v52r2
===

This version uses LHCb v44r2p1, Geant4 **v104r2**, Gaudi v29r4 and LCG 93 with
 HepMC 2.06.09 and Root 6.12.06

The generators used via LCG_93/MCGenerators) are
 pythia8 235 (with LHCb Tune1), lhapdf 6.1.6.cxxstd, photos++ 3.56, 
 tauola++ 1.1.6b.lhcb, pythia 6.427.2,
 hijing 1.383bs.2, crmc 1.5.6 (epos), alpgen 2.1.4, powhegbox r3043.lhcb, 
 herwig++ 2.7.1, thepeg 2.1.1, 
 rivet 2.6.0, yoda 1.6.7.
 startlight r300  
and the internal implementation of:
 EvtGen with EvtGenExtras, AmpGen, Mint,
 BcVegPy and GenXicc, SuperChic2, LPair, MIB

The data packages specific for the simulation are 
Geant4Files v104r* , GDMLData v1r* , XmlVis v2r* , DecFiles v30r* , 
LHAPDFSets v2r* , BcVegPyData v2r* , GenXiccData v3r* , PGunsData v1r* , 
MIBData v3r*


<p>
This version is released on `master`. 
<p>
It is to be used for productions referred to as **Sim10Dev** and is for tuning, 
calibration and setting up and cross check in view of Sim10.
<p>
The only difference between this version and v52r1 is the more recent version of
Geant4 *10.4.p02* used.


### Monitoring changes and Changes to tests

- Test for Sim10Dev, !314 (@gcorti)   
  Changed reference tests due to change on patch of Geant4 version to use (10.4.p02)


